module.exports = (sequelize, Sequelize) => {
    const Events = sequelize.define("events", {
        id: {
            type: Sequelize.FLOAT,
            primaryKey: true
        },
        type: {
            type: Sequelize.STRING
        },
        actor: {
            type: Sequelize.JSONB
        },
        repo: {
            type: Sequelize.JSONB
        },
        created_at: {
            type: Sequelize.STRING
        },
    }, {
        timestamps: false
    });

    return Events;
};