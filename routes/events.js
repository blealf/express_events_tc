const express = require('express');
var router = express.Router();
const events = require('../controllers/events');

// Routes related to event
router.post('/', events.addEvent);

router.get('/', events.getAllEvents);

router.delete('/', events.eraseEvents);

router.get('/actors/:id', events.getByActor)

module.exports = router;