var express = require('express');
var router = express.Router();
const actors = require('../controllers/actors');
// Routes related to actor.

router.put('/:id', actors.updateActor);

router.get('/', actors.getAllActors);

router.get('/streak', actors.getStreak);

module.exports = router;