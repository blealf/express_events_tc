const db = require("../models");
const Event = db.events;
const Op = db.Sequelize.Op;

// ----- Returning the actor records ordered by the total number of events: -----
const getAllActors = (req, res) => {

	Event.findAll({ 
		attributes: [
			'actor', [db.sequelize.fn('MAX', db.sequelize.col('created_at')), 'latest_event'],
			[db.sequelize.fn('COUNT', db.sequelize.col('actor')), 'event_count']
		],
		group: 'actor',
		order: [[ db.sequelize.literal('event_count'), 'DESC'], [db.sequelize.literal('latest_event'), 'DESC'], ['actor.login', 'ASC']]
	})
	.then(data => {
		res.status(200).send(data);
	}).catch(err => {
		res.status(500).send({
			message: 
				err.message || "Some error occurred while retrieving the actors"
		})
	});
};

// ------- Updating the avatar URL of the actor -------
const updateActor = (req, res) => {
	if(!req.body){
    res.status(400).send({
      message: "Data for update cannot be empty"
    })
		return;
  }
	
  const actor_id = req.params.id;

	Event.findOne({ where: { ["actor.id"] : actor_id }})
	.then(event => {
		if(!event) res.status(404).send({ message: `Actor with id ${actor_id} was not found`});
		else {
			Event.update({
				actor : {
					'avatar_url': req.body.avatar_url,
					'id': req.body.id || event.actor.id,
					login: req.body.login || event.actor.login
				}
			}, { where: { ["actor.id"] : actor_id }})
			.then(data => {
				if (req.body.id || req.body.login) res.status(400).send(data);
				else res.status(200).send(data);
			})
		}
	}).catch(err => {
		res.status(500).send({
			message: 
				err.message || "Some error occurred while updating the actors"
		});
	});
 
};

// ----- Returning the actor records ordered by the maximum streak -----
const getStreak = (req, res) => {
		const query = `
		select distinct on (actor) actor, count(distinct created_at::timestamp::date) as streak
			from (select e.*,
									dense_rank() over (partition by actor order by created_at::timestamp::date) as seq
						from events e
					) e
			group by actor, created_at::timestamp::date - seq * interval '1 day'
			order by actor, streak desc`

	db.sequelize.query(query)
		.then(data => {
			if(!data) res.status(400).send({ message: ""});
			else res.status(200).send(data[0]);
		}).catch(err => {
			res.status(500).send({
				message: 
					err.message || "Some error occurred while retrieving the actors"
			})
		});
};


module.exports = {
	updateActor: updateActor,
	getAllActors: getAllActors,
	getStreak: getStreak
};

















