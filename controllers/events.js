const db = require("../models");
const Event = db.events;
const Op = db.Sequelize.Op;

// ----- Returning all events ------
const getAllEvents = (req, res) => {
	Event.findAll({ order: [['id', 'ASC']]})
		.then(data => {
			res.status(200).send(data);
		})
		.catch(err => {
			res.status(500).send({
				message:
					err.message || "Some error occurred while retrieving the events"
			})
		})
};

// ------ Adding new events ------
const addEvent = (req, res) => {
	const event = {
		id: req.body.id,
		type: req.body.type,
		actor: req.body.actor,
		repo: req.body.repo,
		created_at: req.body.created_at
		// created_at: new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '')
	}

	// function to check if event with id is present in database
	const isIdPresent = (id) => {
    return Event.count({ where: { id: id } })
      .then(count => {
        if (count != 0) {
          return false;
        }
        return true;
    });
	}

	isIdPresent(event.id).then(isPresent => {
    if (!isPresent) {
			res.status(400).send({
				message:
					`Event with id ${event.id} already exists`
			});
			return;
    } else {
			Event.create(event)
			.then(data => {
				res.status(201).send(data);
			})
			.catch(err => {
				res.status(500).send({
					message:
						err.message || "Some error occurred while creating the Event."
				});
			});
		}
	})

	
};

// ----- Returning the event records filtered by the actor ID -------
const getByActor = (req, res) => {
	const actor_id = req.params.id

	Event.findAll({ where: { ["actor.id"]: actor_id}, order: [['id', 'DESC']]})
		.then(data => {
			if (!data) res.status(404).send({ message: "Actor id does not exist" });
			else res.status(200).send(data);
		}).catch(err => {
			res.status(500).send({
				message:
				 err.message || "Some error occurred while retrieving the events"
			})
		})
};

// ------ Erasing all events ------
const eraseEvents = (req, res) => {
	Event.destroy({
    where: {},
    truncate: false
  })
    .then(nums => {
      res.status(200).send({ message: `${nums} Event(s) were deleted successfully!` });
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while removing all Event(s)."
      });
    });
};

module.exports = {
	getAllEvents: getAllEvents,
	addEvent: addEvent,
	getByActor: getByActor,
	eraseEvents: eraseEvents
};

















